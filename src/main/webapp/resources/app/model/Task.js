Ext.define('app.model.Task', {
    extend: 'Ext.data.Model',
    fields: ['id', 'name', 'description',
             {name: 'dueDate', type: 'date', dateFormat: 'time', serialize: function (v, r) {
                 return new Date(v).getTime();
             }},
             {name: 'priority', type: 'integer'},
             {name: 'done', type: 'boolean'}, {name: 'username'}],
    proxy: {
        type: 'rest',
        url: '/todoist/api/tasks'
    },
    associations: [
        { type: 'belongsTo', model: 'User' }
    ]
});

