Ext.define('app.model.User', {
    extend: 'Ext.data.Model',
    fields: [/*{name: 'id', type: 'int'},*/ 'firstName', 'lastName', 'password', 'username'],
    proxy: {
        type: 'rest',
        url: '/todoist/api/users'
    }
});

