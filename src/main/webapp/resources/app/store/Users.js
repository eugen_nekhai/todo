Ext.define('app.store.Users', {
    pageSize: 10,
    extend: 'Ext.data.Store',
    requires: 'app.model.User',
    model: 'app.model.User'
});
