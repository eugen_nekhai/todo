Ext.define('app.store.Tasks', {
    pageSize: 10,
    extend: 'Ext.data.Store',
    requires: 'app.model.Task',
    model: 'app.model.Task'
});
