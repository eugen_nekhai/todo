Ext.define('app.controller.TaskController', {
    extend: 'Ext.app.Controller',
    stores: ['Tasks'],
    views: ['TasksGrid', 'TaskWindow', 'NewUser'],
    requires: ['app.view.TaskWindow'],
    refs: [
        {
            ref: 'taskWindow',
            selector: 'taskwindow',
            autoCreate: true,
            xtype: 'taskwindow'
        },
        {
            ref: 'userWindow',
            selector: 'newuser',
            autoCreate: true,
            xtype: 'newuser'
        }
    ],

    init: function () {
        this.control({
            'gridpanel': {
                editTask: this.onTaskEdit,
                addTask: this.onTaskAdd,
                newTask: this.onNewTask,
                deleteTask: this.onTaskDelete,
                newUser: this.onNewUser,
                logout: this.doLogout

            },
            'taskwindow': {
                addTask: this.onTaskAdd
            }
        });


    },
    onNewTask: function () {
        var wnd = this.getTaskWindow();
        wnd.down('form').loadRecord(Ext.create('app.model.Task', {}));
        wnd.show();
    },

    onTaskAdd: function (record) {
        var me = this;
        record.save(
            {
                success: function (rec) {
                    me.getTasksStore().add(rec);
                    me.getTaskWindow().close();
                }
            }
        );
    },
    onTaskDelete: function (record) {
        record.destroy();
    },
    onTaskEdit: function (record) {
        var wnd = this.getTaskWindow();
        wnd.down('form').setTask(record);
        wnd.show();
    },
    doLogout: function () {
        Ext.Ajax.request({
            url: 'api/logout',
            method: 'GET',
            success: function (response) {
                document.location = 'http://localhost:8080/todoist/index.html';
            }
        });
    },
    onNewUser: function () {
        var wnd = this.getUserWindow(), form = this.getUserWindow().down('form');

        wnd.show();

    }
})
;