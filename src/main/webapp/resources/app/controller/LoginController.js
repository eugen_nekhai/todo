Ext.define('app.controller.LoginController', {
    extend: 'Ext.app.Controller',
    views: ['Login'],
    requires: ['app.view.Login'],
    refs: [
        {
            ref: 'loginWindow',
            selector: 'login',
            autoCreate: true,
            xtype: 'login'
        },
    ],

    init: function () {
        this.control({
            'login': {
                login: this.onLogin
            }
        });


    },
    onLogin: function () {
        var wnd = this.getLoginWindow();
        wnd.close();

        Ext.create('app.view.Viewport');
    }
})
;