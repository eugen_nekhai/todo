Ext.define('app.view.Viewport', {
    requires: [
        'app.view.TasksGrid'
    ],

    extend: 'Ext.container.Viewport',
    layout: 'fit',

    items: [
        {
            xtype: 'tasksgrid'
        }
    ]
});