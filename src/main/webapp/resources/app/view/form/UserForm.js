Ext.define('app.view.form.UserForm', {
    extend: 'Ext.form.Panel',
    alias: 'widget.userform',

    bodyPadding: 5,
    width: 350,

    layout: 'anchor',
    defaults: {
        anchor: '100%'
    },
    config: {
        task: null
    },
    defaultType: 'textfield',
    initComponent: function () {
        var me = this;
        me.items = [
            {
                fieldLabel: 'UserName',
                name: 'username',
                allowBlank: false
            },
            {
                fieldLabel: 'Password',
                name: 'password',
                allowBlank: false
            },
            {
                fieldLabel: 'First Name',
                name: 'firstName',
                allowBlank: true
            },
            {
                fieldLabel: 'Last Name',
                name: 'lastName',
                allowBlank: true
            }
        ];
        me.buttons = [
            {
                text: 'Ok',
                formBind: true,
                disabled: true,
                handler: function () {
                    var form = this.up('form').getForm(), wnd = form.owner.up('window');
                    if (form.isValid()) {
                        var newusr = Ext.create('app.model.User', {})
                        form.updateRecord(newusr);

                        newusr.save(
                            {
                                success: function () {
                                    wnd.close();
                                }
                            }
                        );

                    }
                }
            },
            {
                text: 'Cancel',
                formBind: true,
                disabled: true,
                handler: function () {
                    var form = this.up('form').getForm();
                    form.owner.up('window').close()
                }
            }
        ];

        me.callParent();
    }

});