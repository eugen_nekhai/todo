Ext.define('app.view.form.TaskForm', {
    extend: 'Ext.form.Panel',
    alias: 'widget.taskform',

    bodyPadding: 5,
    width: 350,

    layout: 'anchor',
    defaults: {
        anchor: '100%'
    },
    config: {
       task: null
    },
    defaultType: 'textfield',
    initComponent: function () {
        var me = this;
        me.items = [
            {
                fieldLabel: 'Task Name',
                name: 'name',
                allowBlank: false
            },
            {
                fieldLabel: 'Description',
                name: 'description',
                allowBlank: true
            },
            {
                xtype: 'combo',
                fieldLabel: 'Priority',
                store: [0, 1, 2, 3, 4, 5],
                queryMode: 'local',
                name: 'priority',
                allowBlank: true
            },
            {
                xtype: 'datefield',
                anchor: '100%',
                fieldLabel: 'Date',
                name: 'dueDate',
                format: 'd-m-Y H:i',
//                value: '2-4-1978 00:00',
                allowBlank: false
            },
            {
                xtype: 'checkbox',
                fieldLabel: 'Done',
                name: 'done'
            }
        ];
        me.buttons = [
            {
                text: 'Ok',
                formBind: true,
                disabled: true,
                handler: function () {
                    var form = this.up('form').getForm(), wnd = form.owner.up('window');
                    if (form.isValid()) {
                        var rec = this.up('form').getTask();
                        if (rec) {
                            form.updateRecord(rec);
                            rec.save(
                                {
                                    success: function () {
                                        wnd.close();
                                        rec.commit();
                                    }
                                }
                            );

                        } else {
                            var newrec = Ext.create('app.model.Task', {});
                            form.updateRecord(newrec);

                            wnd.fireEvent('addTask', newrec);
                        }
                    }
                }
            },
            {
                text: 'Cancel',
                formBind: true,
                disabled: true,
                handler: function () {
                    var form = this.up('form').getForm();
                    form.owner.up('window').close()
                }
            }
        ];

        me.callParent();
    },
    applyTask: function (task) {
        this.task = task;
        this.loadRecord(task);
    }

});