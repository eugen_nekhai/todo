Ext.define('app.view.NewUser', {
    extend: 'Ext.window.Window',
    alias: 'widget.newuser',

    requires: ['app.view.form.UserForm'],

    title: 'New User',
    height: 200,
    width: 400,
    layout: 'fit',
    initComponent: function () {
        var me = this;

        me.items = {
            xtype: 'userform'
        }

        me.callParent();
    }
});