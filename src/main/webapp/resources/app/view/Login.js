Ext.define('app.view.Login', {
    extend: 'Ext.window.Window',

    alias: 'widget.login',
    width: 350,
    modal: false,
    preventHeader: false,
    floating: true,
    closable: false,
    shadow: true,
    buttonAlign: 'left',
    draggable: false,
    resizable: false,
    title: 'Login',
    layout: {
        type: 'hbox',
        pack: 'center'
    },
    initComponent: function () {
        var me = this,
            E = Ext;

        me.username = E.create('Ext.form.field.Text', {
            fieldLabel: 'User Id',
            labelSeparator: '',
            labelAlign: 'top',
            name: 'userid',
            allowBlank: false,
            blankText: 'user id'
        });

        me.password = E.create('Ext.form.field.Text', {
            labelAlign: 'top',
            fieldLabel: 'Password',
            labelSeparator: '',
            name: 'password',
            inputType: 'password',
            allowBlank: false,
            blankText: 'password'
        });


        me.login = new E.FormPanel({
            border: false,
            bodyStyle: 'padding:10px;',
            url: 'api/user/login',
            defaultType: 'textfield',
            labelWidth: 90,

            defaults: {
                width: 300

            },
            items: [
                me.username, me.password
            ],

            parentView: me
        });

        me.headPanel = new E.Container({
            baseCls: 'x-plain',
            width: 220,
            height: 30
        });

        me.logo = E.create('Ext.Container', {
            layout: {
                type: 'vbox'
            },
            height: 90,
            width: 200,
            items: [
                {
                    xtype: 'container',
                    width: 200,
                    height: 30,
                    items: []
                },
                me.headPanel
            ]
        });

        me.items = [me.login];

        me.submitButton = E.create('Ext.Button', {
            text: 'Login',
            formBind: true,
            handler: me.doLogin,
            scope: me
        });

        me.buttons = [me.submitButton, '->'];

        me.callParent();

        me.username.on('specialkey', function (field, event) {
            if (event.getKey() === event.ENTER) {
                me.doLogin();
            }
        }, me);

        me.password.on('specialkey', function (field, event) {
            if (event.getKey() === event.ENTER) {
                me.doLogin();
            }
        }, me);

        me.on('afterrender', function () {
            if (me.username.getValue() === '') {
                me.username.focus(true, 300);
            } else {
                me.password.focus(true, 300);
            }
        }, me);
    },

    /**
     * Perform login action
     */
    doLogin: function () {
        var me = this;

        me.login.getForm().submit({
            method: 'POST',
            url: 'api/user/login',
            success: function (form, action) {
                var userid = me.username.value

                Ext.getStore('Tasks').load({
                    params: {
                        userid: userid
                    }
                });

                Ext.getStore('Users').load();

                me.fireEvent('login', {});
            },

            failure: function () {
            }
        });

    }

});
