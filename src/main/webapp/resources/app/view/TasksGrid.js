Ext.define('app.view.TasksGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.tasksgrid',

    requires: ['Ext.toolbar.Toolbar', 'Ext.toolbar.TextItem', 'app.view.TaskWindow'],

    layout: 'fit',

    store: 'Tasks',
    title: 'Tasks',

    selType: 'checkboxmodel',
    selModel: {
        mode: 'SINGLE',
        ignoreRightMouseSelection: false
    },

    initComponent: function () {
        var me = this;

        me.items = [
            {
                layout: {
                    type: 'hbox',
                    align: 'stretch'
                },
                items: [


                ]
            }
        ];
        me.columns = [
            {
                header: 'ID',
                dataIndex: 'id'
            },
            {
                header: 'Name',
                dataIndex: 'name',
                editor: 'textfield'
            },
            {
                header: 'Description',
                dataIndex: 'description',
                editor: 'textfield',
                flex: 1
            },
            {
                xtype: 'datecolumn',
                header: 'Due Date',
                dataIndex: 'dueDate',
                format: 'd-m-Y H:i',
                flex: 1
            },
            {
                header: 'Priority',
                dataIndex: 'priority'
            },
            {
                xtype: 'checkcolumn',
                text: 'Done',
                dataIndex: 'done',
                stopSelection: false,
                align: 'center',
                defaultType: 'boolean'
            }

        ];
        me.dockedItems = [
            {
                xtype: 'toolbar',
                style: {
                    fontWeight: 'bold',
                    whiteSpace: 'nowrap'
                },
                padding: '2 10 2 10',
                dock: 'top',
                items: [
                    {
                        text: 'Add',
                        handler: function () {
                            var grid = this.up('grid')

                            me.fireEvent('newTask', Ext.create('app.model.Task'));

                        }
                    },
                    {
                        text: 'Delete',
                        handler: function () {
                            var grid = this.up('grid'), record = grid.getSelectionModel().selected.get(0);
//                            record.destroy();
                            me.fireEvent('deleteTask', record);

                        }
                    },
                    {
                        text: 'Edit',
                        handler: function () {
                            var grid = this.up('grid'), record = grid.getSelectionModel().selected.get(0);
                            me.fireEvent('editTask', record);

                        }
                    },
                    '->',
                    {
                        text: 'New user',
                        handler: function () {
                            me.fireEvent('newUser');
                        }
                    },
                    {
                        text: 'Logout',
                        handler: function () {
                            me.fireEvent('logout');

                        }
                    }

                ]

            }
        ];
        me.callParent(arguments);
    },
    listeners: {
        edit: function (editor, object) {
            var record = object.record;
            record.save();
            record.commit();
        }
    }
});

