Ext.define('app.view.TaskWindow', {
    extend: 'Ext.window.Window',
    alias: 'widget.taskwindow',

    requires: ['app.view.form.TaskForm'],

    title: 'New Task',
    width: 400,
    layout: 'fit',
    initComponent: function () {
        var me = this;

        me.items = {
            xtype: 'taskform'
        }

        me.callParent();
    }
});