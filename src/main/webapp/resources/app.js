Ext.Loader.setConfig({
    enabled: true
});

Ext.application({
    name: 'app',
    session: '',
    autoCreateViewport: false,
    controllers: [
        'LoginController',
        'UserController',
        'TaskController'
    ],
    launch: function () {
        app.events = this;

        // Enable History
        Ext.History.init();
        // Enable tooltips
        Ext.tip.QuickTipManager.init();
        Ext.apply(Ext.tip.QuickTipManager.getQuickTip(), {
            showDelay: 50
            // Show 50ms after entering target
        });

        Ext.state.Manager.setProvider(new Ext.state.LocalStorageProvider());

        Ext.util.Observable.observe(Ext.data.Connection);


        Ext.create('app.view.Login').show();
        //Ext.create('app.view.Viewport');
    },

    views: ['Viewport'],

    apiRoot: '/todoist'

});