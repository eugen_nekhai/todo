package com.springapp.mvc.controller;

import com.springapp.mvc.dto.UserDTO;
import com.springapp.mvc.orm.repository.RoleRepository;
import com.springapp.security.domain.Role;
import com.springapp.security.domain.User;
import com.springapp.security.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

@Controller
@RequestMapping("/api/users")
public class UserController {
    @Resource
    private UserRepository userRepository;
    @Resource
    private RoleRepository roleRepository;

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public List<User> getUsers() {
        return userRepository.findAll();
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public User createUser(@RequestBody UserDTO userDTO) throws InvocationTargetException,
            IllegalAccessException {
        User user = userRepository.save(buildUser(userDTO));

        Role role = new Role();
        role.setUser(user);
        role.setRole(1);

        roleRepository.save(role);
        return user;
    }

    private User buildUser(UserDTO userDTO) {
        User user = new User();

//        user.setId(userDTO.getId());
        user.setUsername(userDTO.getUsername());
        user.setPassword(userDTO.getPassword());

        Role role = roleRepository.findOne(1L);

//        user.setRole(role);

        return user;
    }

}
