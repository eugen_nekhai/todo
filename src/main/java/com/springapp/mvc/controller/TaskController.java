package com.springapp.mvc.controller;

import com.springapp.mvc.dto.TaskDTO;
import com.springapp.mvc.exception.TaskNotFoundException;
import com.springapp.mvc.orm.model.Task;
import com.springapp.mvc.orm.service.TaskService;
import com.springapp.security.domain.User;
import com.springapp.security.repository.UserRepository;
import com.springapp.security.util.SecurityContextUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.annotation.Resource;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Slf4j
@Controller
@RequestMapping("/api/tasks")
public class TaskController {
    @Resource
    private TaskService taskService;

    @Resource
    private UserRepository userRepository;

    @ResponseBody
    @RequestMapping(method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public List<Task> getAllTasks(@RequestParam String userid) throws TaskNotFoundException {
        User user = userRepository.findByUsername(userid);

        return taskService.findByUser(user);
    }

    @ResponseBody
    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public Task addTask(@RequestBody TaskDTO taskDTO) throws InvocationTargetException, IllegalAccessException, TaskNotFoundException {
        return taskService.add(taskDTO);
    }

    @ResponseBody
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    public Task updateTask(@PathVariable Long id, @RequestBody TaskDTO taskDTO) throws InvocationTargetException,
            IllegalAccessException, TaskNotFoundException {
        return taskService.update(taskDTO);
    }

    @ResponseBody
    @RequestMapping(value = "/{taskId}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteTask(@PathVariable Long taskId) throws TaskNotFoundException {
        taskService.deleteById(taskId);
    }

    @ExceptionHandler(TaskNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public void handleTodoNotFoundException(TaskNotFoundException ex) {
        log.debug("handling 404 error on a todo entry");
    }
}