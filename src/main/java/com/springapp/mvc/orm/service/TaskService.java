package com.springapp.mvc.orm.service;


import com.springapp.mvc.dto.TaskDTO;
import com.springapp.mvc.exception.TaskNotFoundException;
import com.springapp.mvc.orm.model.Task;
import com.springapp.security.domain.User;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

public interface TaskService {

    public Task add(TaskDTO added) throws InvocationTargetException, IllegalAccessException;

    public Task deleteById(Long id) throws TaskNotFoundException;

    public List<Task> findAll();

    public Task findById(Long id) throws TaskNotFoundException;

    public List<Task> findByUser(User id) throws TaskNotFoundException;

    public Task update(TaskDTO updated) throws TaskNotFoundException, InvocationTargetException, IllegalAccessException;
}