package com.springapp.mvc.orm.service;

import com.springapp.mvc.dto.TaskDTO;
import com.springapp.mvc.exception.TaskNotFoundException;
import com.springapp.mvc.orm.model.Task;
import com.springapp.mvc.orm.repository.TaskRepository;
import com.springapp.security.domain.User;
import com.springapp.security.repository.UserRepository;
import com.springapp.security.util.SecurityContextUtil;
import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

@Service
public class RepositoryTaskService implements TaskService {

    @Autowired
    private TaskRepository repository;
    @Resource
    private UserRepository userRepository;

    @Resource
    private SecurityContextUtil contextUtil;

    @Transactional
    @Override
    public Task add(TaskDTO added) throws InvocationTargetException, IllegalAccessException {
        Task task = new Task();

        String userName = contextUtil.getPrincipal().getUsername();
        User user = userRepository.findByUsername(userName);
        BeanUtils.copyProperties(task, added);
        task.setUser(user);
        return repository.save(task);
    }

    @Transactional(rollbackFor = {TaskNotFoundException.class})
    @Override
    public Task deleteById(Long id) throws TaskNotFoundException {
        Task deleted = findById(id);

        repository.delete(deleted);
        return deleted;
    }

    @Transactional(readOnly = true)
    @Override
    public List<Task> findAll() {
        return repository.findAll();
    }

    @Transactional(readOnly = true)
    @Override
    public List<Task> findByUser(User id) {
        return repository.findByUser(id);
    }

    @Transactional(readOnly = true, rollbackFor = {TaskNotFoundException.class})
    @Override
    public Task findById(Long id) throws TaskNotFoundException {
        Task found = repository.findOne(id);

        if (found == null) {
            throw new TaskNotFoundException("No to-entry found with id: " + id);
        }

        return found;
    }


    @Transactional(rollbackFor = {TaskNotFoundException.class})
    @Override
    public Task update(TaskDTO updated) throws TaskNotFoundException, InvocationTargetException, IllegalAccessException {
        Task model = findById(updated.getId());

        BeanUtils.copyProperties(model, updated);

        repository.save(model);
        return model;
    }
}