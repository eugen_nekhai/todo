package com.springapp.mvc.dto;

import lombok.Data;
import org.codehaus.jackson.annotate.JsonAutoDetect;

@Data
@JsonAutoDetect
public class UserDTO {
    private Long id;

    private String firstName;
    private String lastName;

    private String username;
    private String password;

    private Integer role;

    public Integer getRole() {
        return 1;
    }
}
