package com.springapp.mvc.dto;

import lombok.Data;
import org.codehaus.jackson.annotate.JsonAutoDetect;

import java.util.Date;

@Data
@JsonAutoDetect
public class TaskDTO {
    private Long id;
    private String name;
    private String description;
    private Date dueDate;
    private Integer priority;
    private Boolean done;
    private String username;
}
